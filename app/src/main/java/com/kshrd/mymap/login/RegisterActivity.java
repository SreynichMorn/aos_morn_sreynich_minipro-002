package com.kshrd.mymap.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.kshrd.mymap.MainActivity;
import com.kshrd.mymap.R;

public class RegisterActivity extends AppCompatActivity {
    Button btnRegister;
    EditText inputEmail,inputPass,inputName;
    TextView textView;
    FirebaseAuth mFirebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirebaseAuth=FirebaseAuth.getInstance();
        inputEmail=findViewById(R.id.et_email);
        inputPass=findViewById(R.id.et_reg_password);
        inputName=findViewById(R.id.et_name);
        btnRegister=findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email,password,name;
                email=inputEmail.getText().toString();
                password=inputPass.getText().toString();
                name=inputName.getText().toString();
                if(email.isEmpty()){
                    inputEmail.setError("Please enter your email");
                    inputEmail.requestFocus();
                }
                else if(password.isEmpty()){
                    inputPass.setError("Please enter your password");
                    inputPass.requestFocus();
                }
                else if(name.isEmpty()){
                    inputName.setError("Please enter your name");
                    inputName.requestFocus();
                }
                else if(email.isEmpty() && password.isEmpty() && name.isEmpty()){
                    Toast.makeText(RegisterActivity.this,"Fields are empty...",Toast.LENGTH_LONG).show();
                }
                else if(!(email.isEmpty() && password.isEmpty() && name.isEmpty())){
                    mFirebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(RegisterActivity.this,"SignUp Unsuccessfully, Please Try Again",Toast.LENGTH_LONG).show();
                            }
                            else {
                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                            }
                        }
                    });
                }
                else {
                    Toast.makeText(RegisterActivity.this,"Error Occurred",Toast.LENGTH_LONG).show();
                }
            }
        });
        
    }
}