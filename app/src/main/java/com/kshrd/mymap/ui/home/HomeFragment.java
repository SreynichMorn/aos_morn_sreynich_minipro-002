package com.kshrd.mymap.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;

import com.google.firebase.auth.FirebaseUser;
import com.kshrd.mymap.GetNearbyPlacesData;
import com.kshrd.mymap.R;
import com.skyfishjy.library.RippleBackground;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener, SeekBar.OnSeekBarChangeListener {

    private HomeViewModel homeViewModel;

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    int PROXIMITY_RADIUS=1000;
    double latitude,longitude;
    double end_latitude,end_longitude;
    private PlacesClient placesClient;
    SupportMapFragment mapFragment;
    private List<AutocompletePrediction> predictionList;
    CheckBox checkBox;
    SeekBar seekRed,seekGreen;
    Button btDraw,btClear;
    int red=0,green=0,blue=0;
    Polygon polygon=null;
    List<LatLng>latLngList=new ArrayList<>();
    List<Marker>markerList=new ArrayList<>();
    private Location currentLocation;
    SearchView searchView;
    GoogleApiClient client;
    private View mapView;
    private Button btnFind;
    private RippleBackground rippleBg;

    private final float DEFAULT_ZOOM = 15;
    private final int REQUEST_CODE = 6;
    TextView txtMap, txtSatellite;


    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        @SuppressLint({"MissingPermission", "PotentialBehaviorOverride"})
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap=googleMap;


            if(currentLocation==null){
//                Toast.makeText(getActivity(),"NO DATA",Toast.LENGTH_LONG).show();
            }
            else {
                LatLng latLng=new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude());
                MarkerOptions markerOptions=new MarkerOptions().position(latLng)
                        .title("I AM HERE");
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                mMap.addMarker(markerOptions);
            }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                MarkerOptions markerOptions=new MarkerOptions().position(latLng);



//                Marker marker=mMap.addMarker(markerOptions);
                latLngList.add(latLng);
//                markerList.add(marker);
            }
        });

        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        txtMap=root.findViewById(R.id.txtMap);
        txtSatellite=root.findViewById(R.id.txtSatellite);
        checkBox=root.findViewById(R.id.checkbox);
        seekRed=root.findViewById(R.id.seek_red);
        seekGreen=root.findViewById(R.id.seek_green);
        btDraw=root.findViewById(R.id.bt_draw);
        btClear=root.findViewById(R.id.bt_clear);

        HomeFragment fragment = (HomeFragment) getFragmentManager().findFragmentById(R.id.map);
        searchView=root.findViewById(R.id.searchLocation);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        txtMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                txtMap.setBackgroundResource(R.drawable.map);
                txtSatellite.setVisibility(View.VISIBLE);
                txtMap.setVisibility(View.GONE);
            }
        });
        txtSatellite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                txtSatellite.setBackgroundResource(R.drawable.satellite);
                txtMap.setVisibility(View.VISIBLE);
                txtSatellite.setVisibility(View.GONE);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

        mapView=getView();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        Places.initialize(getActivity(), getString(R.string.google_maps_key));
        placesClient = Places.createClient(getContext());
        final AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        fetchLastLocation();
        onClick();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                String location=searchView.getQuery().toString();
                List<Address>addressList=null;
                if(location !=null || location.equals("")){
                    Geocoder geocoder=new Geocoder(getActivity());
                    try {
                        addressList=geocoder.getFromLocationName(location,1);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if(addressList.size()!=0){
                        Address address=addressList.get(0);
                        LatLng latLng=new LatLng(address.getLatitude(),address.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(latLng).title(location));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                    }
                    else {
                        Toast.makeText(getActivity(),"NO DATA",Toast.LENGTH_LONG).show();
                    }
                }

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        mapFragment.getMapAsync(callback);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(polygon==null) return;
                    polygon.setFillColor(Color.rgb(red,green,blue));
                }
            }
        });
        btDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(polygon !=null)polygon.remove();
                PolygonOptions polygonOptions=new PolygonOptions().addAll(latLngList)
                        .clickable(true);
                polygon=mMap.addPolygon(polygonOptions);
                polygon.setStrokeColor(Color.rgb(red,green,blue));
                if(checkBox.isChecked())
                    polygon.setFillColor(Color.rgb(red,green,blue));
            }
        });
        btClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(polygon !=null)polygon.remove();
                for(Marker marker:markerList)marker.remove();
                latLngList.clear();
                markerList.clear();
                checkBox.setChecked(false);
                seekRed.setProgress(0);
                seekGreen.setProgress(0);
            }
        });
        seekRed.setOnSeekBarChangeListener(this);
        seekGreen.setOnSeekBarChangeListener(this);
    }

    private void onClick() {
        Object dataTransfer[]=new Object[2];
        GetNearbyPlacesData getNearbyPlacesData=new GetNearbyPlacesData();
        switch (mapView.getId()){
            case R.id.hospital:
                mMap.clear();
                String hospital="hospital";
                String url=getUrl(latitude,longitude,hospital);

                dataTransfer[0]=mMap;
                dataTransfer[1]=url;
                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(getActivity(),"Showing Nearby Hospitals",Toast.LENGTH_LONG).show();
                break;
            case R.id.school:
                mMap.clear();
                String school="school";
                url=getUrl(latitude,longitude,school);
                dataTransfer[0]=mMap;
                dataTransfer[1]=url;
                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(getActivity(),"Showing Nearby School",Toast.LENGTH_LONG).show();
                break;
            case R.id.restaurant:
                mMap.clear();
                String restaurant="restaurant";
                url=getUrl(latitude,longitude,restaurant);
                dataTransfer[0]=mMap;
                dataTransfer[1]=url;
                getNearbyPlacesData.execute(dataTransfer);
                Toast.makeText(getActivity(),"Showing Nearby Restaurant",Toast.LENGTH_LONG).show();
                break;
        }
    }
    private String getUrl(double latitude,double longitude,String nearbyPlace){
        StringBuilder googlePlaceUrl=new StringBuilder("https://maps.googleapis.com/maps/api/place/textsearch/json?");
        googlePlaceUrl.append("location"+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&type="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyBq7S-Z1Lf5jl1w0errW99OAAi1kEj49Nw");
        return googlePlaceUrl.toString();
    }


    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_CODE);
            return;
        }
        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null){
                    currentLocation=location;
                    Toast.makeText(getContext(),currentLocation.getLatitude()
                    +""+currentLocation.getLongitude(),Toast.LENGTH_LONG).show();
                    SupportMapFragment mapFragment =
                            (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    if (mapFragment != null) {
                        mapFragment.getMapAsync(callback);
                    }
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){

            case REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    fetchLastLocation();
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + requestCode);
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.setDraggable(true);
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        end_latitude=marker.getPosition().latitude;
        end_longitude=marker.getPosition().longitude;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        switch (seekBar.getId()){
            case  R.id.seek_red:
                red=progress;
                break;
            case  R.id.seek_green:
                red=progress;
                break;
        }
        if(polygon!=null){
            polygon.setStrokeColor(Color.rgb(red,green,blue));
            if (checkBox.isChecked())
                polygon.setFillColor(Color.rgb(red,green,blue));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}